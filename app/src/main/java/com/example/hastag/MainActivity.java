package com.example.hastag;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;

import com.example.hastag.Adapter.HashtagAdapter;
import com.example.hastag.Model.Hashtag;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Hashtag> hashtagList;
    private RecyclerView listCategoryHashtag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

        setListHashTag();


    }

    ItemClickListener itemClickListener = (view, position) -> {
        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
        intent.putExtra("LIST", hashtagList.get(position));
        startActivity(intent);

    };

    private void setListHashTag() {
        hashtagList = new Gson().fromJson(loadJSONFromAsset(), new TypeToken<List<Hashtag>>() {
        }.getType());
        listCategoryHashtag.setLayoutManager(new LinearLayoutManager(this));
        listCategoryHashtag.setAdapter(new HashtagAdapter(hashtagList, MainActivity.this, itemClickListener));

    }

    public String loadJSONFromAsset() {
        String json;
        try {
            InputStream is = getApplicationContext().getAssets().open("json_hashtag.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
    private void init() {
        hashtagList = new ArrayList<>();
        listCategoryHashtag = findViewById(R.id.list_hastag);
    }


}